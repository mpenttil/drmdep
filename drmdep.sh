#!/bin/bash
#
# drmdep.sh <revision range> [ dir1 dir2 ..]
#
# outputs commit ids for drm subtree which also touch files under
# any of the dirs. If the dirs are omitted, a default of mm is used.
#

DEFAULT_DEPS="mm/"

print_usage() {
        echo "Usage: drmdep <revision range> [<dir/>..]"
}

[[ $# -lt 1 ]] && print_usage

DEPS="${*:2:$#}"
[ -z "$DEPS" ] && DEPS=$DEFAULT_DEPS

A=$(git log --pretty="%H\n" "$1" drivers/gpu/drm/)

function ff {
echo -e $A |
    while read in; do
	 [ -z "$in" ] || git diff-tree -r "$in" --  ${DEPS} | head -n 1

    done
}

AA=$(ff)
echo -e $AA| tr " " "\n"

